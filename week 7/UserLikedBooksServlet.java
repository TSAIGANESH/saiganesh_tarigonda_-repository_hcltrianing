package com.greatlearning.controller.user;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greatlearning.dao.user.UserDao;
import com.greatlearning.model.user.UserModel;

public class UserLikedServlet extends HttpServlet {
private UserDao userDao;
    
    public void init() {
    	try {
			userDao = new UserDao();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
    }
    
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doGet(request,response);
    }
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	    	
    	String id = request.getParameter("likedbookid");
		User user = UserLogin.currentUser;
		String name = user.getName();
		String password = user.getPassword();
		
		try {
			userDao.addLiked(name, password, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body><h1>Book added successfully to liked section</h1></body></html>");

		
	}

}
