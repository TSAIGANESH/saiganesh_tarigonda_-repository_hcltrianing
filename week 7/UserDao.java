package com.greatlearning.dao.user;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.greatlearning.model.user.*;

public class UserDao {
	private Connection con;	
	private PreparedStatement st;
	public UserDao() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookess","root","Sai0140@");
	}
	
	public void userRegisteration(UserModel user) throws SQLException {
		int count;
		st = con.prepareStatement("insert into user values(?,?)");
		st.setString(1, user.getName());
		st.setString(2, user.getPassword());
		count = st.executeUpdate();
		st.close();
		
	
	}
	
	public Boolean getUser(String name, String password) throws SQLException {
		st = con.prepareStatement("select *from user");
		ResultSet rs = st.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password)) {
				st.close();
				return true;	
			}
		}
		st.close();
		return false;
		
	}
	
	public void addLater(String name, String password, String bookId) throws SQLException {
		st = con.prepareStatement("insert into ReadLater(name,password,booksId) values(?,?,?)");
		PreparedStatement st2 = con.prepareStatement("select *from ReadLater");
		ResultSet rs = st2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
				return;	
			}
		}
		st.setString(1, name);
		st.setString(2, password);
		st.setString(3, bookId);
		st.executeUpdate();
		st.close();
		st2.close();
		rs.close();
		
	}
	
	public void addLiked(String name, String password, String bookId) throws SQLException {
		st = con.prepareStatement("insert into liked(name,password,booksId) values(?,?,?)");
		PreparedStatement st2 = con.prepareStatement("select *from liked");
		ResultSet rs = st2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
				return;	
			}
		}
		st.setString(1, name);
		st.setString(2, password);
		st.setString(3, bookId);
		st.executeUpdate();	
		st.close();
		st2.close();
		rs.close();
		
	}
	

}
