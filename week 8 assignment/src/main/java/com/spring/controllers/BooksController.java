package com.spring.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.dao.BookDao;
import com.spring.model.Book;

@Controller
public class BooksController {
	@Autowired
	BookDao bookDao;

	@RequestMapping("/viewBooks")
	public String viewBooks(Model m) {
		List<Book> list = bookDao.getBooks();
		m.addAttribute("list", list);
		return "viewBooks";
	}
}