package com.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.spring.model.Book;

public class BookDao {

	JdbcTemplate template;

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	public List<Book> getBooks() {
		return template.query("select * from books", new RowMapper<Book>() {
			public Book mapRow(ResultSet rs, int row) throws SQLException {
				Book b = new Book();
				b.setBook_id(rs.getInt(1));
				b.setTitle(rs.getString(2));
				b.setTotal_pages(rs.getInt(3));
				b.setRating(rs.getFloat(4));
				b.setIsbn(rs.getInt(5));
				return b;
			}
		});
	}
}
