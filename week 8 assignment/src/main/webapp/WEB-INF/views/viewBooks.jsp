<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ page import="java.util.List"%>
<%@ page import="com.spring.model.*"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view book page</title>

</head>
<body>

		<h1 style="font-size: 80px">Welcome To Library</h1>
		<p>
			<a href="login" style="font-size: 30px">Login</a>
		</p>
		<p>
			<a href="registration" style="font-size: 30px">New Registration</a>
		</p>
		<p>
			<a href="index" style="font-size: 30px">Go To Index</a>
		</p>
		<table border="1" style="background-color: #BDB76B">
			<tr>
				<td>Book_ID</td>
				<td>Book_Name</td>
				<td>Total_Pages</td>
				<td>Rating</td>
				<td>ISBN</td>
			</tr>


			<%
			@SuppressWarnings("unchecked")
			List<Book> book = (List<Book>) request.getAttribute("list");
			%>

			<%
			for (Book b : book) {
			%>

			<tr>
				<td><%=b.getBook_id()%></td>
				<td><%=b.getTitle()%></td>
				<td><%=b.getTotal_pages()%></td>
				<td><%=b.getRating()%></td>
				<td><%=b.getIsbn()%></td>
			</tr>
			<%
			}
			%>
		</table>
</body>
</html>