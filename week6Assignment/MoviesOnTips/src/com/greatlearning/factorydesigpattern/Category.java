package com.greatlearning.factorydesigpattern;

public class Category {
	public static Division setMovieChoice(int choice) {

		if(choice==1) {
			return new UpComing();
		}
		else if(choice==2) {
			return new MoviesOnThreater();
		}
		else if(choice==3) {
			return new TopRatedIndianMovies();
		}
		else if(choice==4) {
			return new TopRatedMovies();
		}
		else {
			return null;
		}
	}

}
