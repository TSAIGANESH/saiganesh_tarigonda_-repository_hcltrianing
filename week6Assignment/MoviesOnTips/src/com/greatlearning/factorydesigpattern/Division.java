package com.greatlearning.factorydesigpattern;

import java.sql.SQLException;
import java.util.List;
import com.greatlearning.model.*;

public interface Division {
public List<Movie> movieChoice() throws SQLException;

	}

