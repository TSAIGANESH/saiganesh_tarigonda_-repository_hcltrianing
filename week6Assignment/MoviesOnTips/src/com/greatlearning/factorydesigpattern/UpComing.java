package com.greatlearning.factorydesigpattern;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.database.*;
import com.greatlearning.model.*;

public class UpComing implements Division{

	Connection con =DatabaseConnection.getInstance().getConnection();

	public UpComing() {

	}
	@Override
	public List<Movie> movieChoice() throws SQLException {

		List<Movie> movies=new ArrayList<Movie>();
		
		String sql="select id,title,year,storyline,rating,classification from moviedataset where Classification ='coming soon'";
		Statement statement =con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{

			Movie movie = new Movie();
			movie.setId(rs.getInt(1));
			movie.setTitle(rs.getString(2));
			movie.setYear(rs.getInt(3));
			movie.setStoryline(rs.getString(4));
			movie.setRating(rs.getInt(5));
			movie.setClassification(rs.getString(6));
			movies.add(movie);
		}

		return movies;	
	}

}

