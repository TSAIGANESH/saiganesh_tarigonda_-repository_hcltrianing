package com.greatlearning.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
	private static DatabaseConnection instance;
	private static Connection con;
	private DatabaseConnection() {

		}
		public static DatabaseConnection getInstance() {
			if(instance == null) {
				instance = new DatabaseConnection();
			}
			return instance;
		}
		public Connection getConnection() {	
			if(con == null) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					con = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/moviesontips","root","Sai0140@");
					System.out.println("Driver Loaded & Connected Successfully");
				} catch (ClassNotFoundException e) {
			
					System.out.println("Error can't Load Driver");
					e.printStackTrace();
				} catch (SQLException e) {
				
					System.out.println("Error in Connecting to Database");
					e.printStackTrace();
				}
			}
			return con;	
		}

	}

