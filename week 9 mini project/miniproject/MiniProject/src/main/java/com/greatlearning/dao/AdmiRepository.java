package com.greatlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.greatlearning.beans.Admin;

public interface AdmiRepository extends JpaRepository<Admin, String> {

}
