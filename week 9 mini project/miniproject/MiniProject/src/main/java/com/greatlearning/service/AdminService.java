package com.greatlearning.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.beans.Admin;
import com.greatlearning.dao.AdmiRepository;

@Service
public class AdminService {
	@Autowired
	AdmiRepository adminDao;
	
   public String adminRegistration(Admin adm) {
	   if(adminDao.existsById(adm.getEmail())) {
		   return "your Details Already Exists...!!!";
	   }else {
		   adminDao.save(adm);
		   return "Admin Details Saved Sucessfully...!!!";
	   }	   
   }
   
   public String checkAdminDetails(Admin adm) {
	   if(adminDao.existsById(adm.getEmail())) {
		   Admin a=adminDao.getById(adm.getEmail());
		   if(a.getPassword().equals(adm.getPassword())) {
			   return "Admin Login  is sucessfully.....!!!!";
		   }else {
			   return "Please Enter Correct Details.....!!!!";
		   }				   
	   }else {
		   return "Your Details Are Not Exists......!!";
	   }
	   
   }

   public List<Admin> getAllAdminAvaliable(){
		return adminDao.findAll();
	}
   
   

   
   
}
