package com.greatlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjectSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectSpringBootApplication.class, args);
		System.out.println("started");
		System.out.println("Server Running on Port number 8080");
	
	}

}
