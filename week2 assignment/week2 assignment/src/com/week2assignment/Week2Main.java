package com.week2assignment;
import java.util.ArrayList;


public class Week2Main {

	public static void main(String[] args) {
		ArrayList<Employee> al=new ArrayList<>();
		  Employee e1 = new Employee(1, "Aman", 20, 1100000, "IT", "Delhi");
		  al.add(e1);
		  Employee e2 = new Employee(2, "Bobby", 22, 500000, "Hr", "Bombay");
		  al.add(e2);
		  Employee e3 = new Employee(3, "Zoe", 20, 750000, "Admin", "Delhi");
		  al.add(e3);
		  Employee e4 = new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai");
		  al.add(e4);
		  Employee e5 = new Employee(5, "Smitha", 24, 1200000, "Hr", "Bengaluru");
		  al.add(e5);
		  System.out.println(al);
		  System.out.println("\n");
		  DataPatternA a = new DataPatternA();
		  a.sortingNames(al);
		  DataPatternB b= new DataPatternB();
		  b.monthlySalary(al);
		  b.cityNameCount(al);
		  
		
	}

}
