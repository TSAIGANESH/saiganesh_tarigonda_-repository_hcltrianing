package com.week2assignment;

import java.util.Objects;

public class Employee {
	int id;
	String name;int age;int salary;
	String dept; String city;
public Employee(int id, String name, int age, int salary, String dept, String city)throws IllegalArgumentExceptionDemo {
	super();
	if(id<0)
	{
		throw new IllegalArgumentExceptionDemo("exception occured,id can't be less than zero");
	}
	this.id=id;
	if(name==null) {
		throw new IllegalArgumentExceptionDemo("exception occured:"+" name can;t be empty"); 
		}
	this.name=name;
	if(age<=0)
	{
		throw new IllegalArgumentExceptionDemo("exception happened:"+"age can't be zero");
	}
	if (salary<0)
	{
		throw new IllegalArgumentExceptionDemo("exception occured:"+"salary can't be zero");
	}
	this.salary=salary;
	if(dept==null)
	{
		throw new IllegalArgumentExceptionDemo("exception happened dept can't be empty");
	}
	this.dept=dept;
	if(city==null)
	{
		throw new IllegalArgumentExceptionDemo("exception occured"+"city can't be empty");
	}
	this.city=city;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public int getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}
public String getDept() {
	return dept;
}
public void setDept(String dept) {
	this.dept = dept;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", dept=" + dept
			+ ", city=" + city + "]";
}
@Override
public int hashCode() {
	return Objects.hash(age, city, dept, id, name, salary);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Employee other = (Employee) obj;
	return age == other.age && Objects.equals(city, other.city) && Objects.equals(dept, other.dept) && id == other.id
			&& Objects.equals(name, other.name) && salary == other.salary;
}
}


               