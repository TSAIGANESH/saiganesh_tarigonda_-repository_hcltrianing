package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.beans.Books;
import com.greatlearning.beans.User;
import com.greatlearning.dao.IBooksRepository;
import com.greatlearning.dao.IUserRepository;
import com.greatlearning.exception.ProjectException;

@Service
public class BooksServiceImpl implements IBooksService {

	@Autowired
	IBooksRepository booksRepository;
	@Autowired
	IUserRepository userRepository;

	@Override
	public Books addBook(Books book) {
		// TODO Auto-generated method stub
		return booksRepository.save(book);
	}

	@Override
	public List<Books> getAllBooks() {
		// TODO Auto-generated method stub
		return booksRepository.findAll();
	}

	@Override
	public Books getBookById(Integer bookId) throws ProjectException {
		// TODO Auto-generated method stub
		return booksRepository.findById(bookId).orElseThrow(() -> new ProjectException("BOOK ID NOT FOUND .....!!!"));
	}

	@Override
	public Books updateBook(Books book) {
		// TODO Auto-generated method stub
		return booksRepository.saveAndFlush(book);
	}

	@Override
	public String deleteBookById(Integer bookId) throws ProjectException {
		// TODO Auto-generated method stub
		Books book = booksRepository.findById(bookId).orElseThrow(() -> new ProjectException("BOOK ID DOESN'T EXISTS.....!!!!"));
		booksRepository.delete(book);
		return "BOOK DELETED SUCCESSFULLY...!!!";
	}

	@Override
	public String addBooksToLike(Integer userId, Integer bookId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("USER ID DOESN'T EXISTS...!!!"));
		Books book = booksRepository.findById(bookId).orElseThrow(() -> new ProjectException("BOOK ID NOT EXISTS....!!!"));
		List<Books> likedBooksList = user.getLikedBooks();
		likedBooksList.add(book);
		user.setLikedBooks(likedBooksList);
		userRepository.save(user);
		return "BOOK ADDED TO LIKE .....!!!!";
	}

	@Override
	public String deleteBooksFromLike(Integer userId, Integer bookId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("USER ID DOESN'T EXISTS...!!!"));
		Books book = booksRepository.findById(bookId).orElseThrow(() -> new ProjectException("BOOK ID NOT EXISTS....!!!"));
		List<Books> likedBooksList = user.getLikedBooks();
		likedBooksList.removeIf(n -> (n.getId() == book.getId()));
		user.setLikedBooks(likedBooksList);
		userRepository.save(user);
		return "BOOK DELETED FROM LIKE...!!!!";
	}

	@Override
	public List<Books> getLikedBooksByUserId(Integer userId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("USER ID DOESN'T EXISTS...!!!"));
		return user.getLikedBooks();
	}

}
